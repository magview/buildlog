import unittest
import buildlog_start_end
import buildlog
from unittest.mock import patch

class TestGetCommits(unittest.TestCase):
    def setUp(self):
        self.cur_version_tagno = "test"
        self.pre_version_tagno = "test"
        self.work_dir = "test"
        self.html_tag_commits = b'test_hashtest_hashtest_hashtest_hashtest <fake_tag>\n'
        self.html_tag_commits += b'test_hashtest_hashtest_hashtest_hashtest <fake_tag2>\n'
        self.html_tag_commits += b'test_hashtest_hashtest_hashtest_hashtest <fake_tag3><fake_tag2><&>\n'
        self.html_tag_commits += b'''test_hashtest_hashtest_hashtest_hashtest <fake_tag3><fake_tag2><&>'"\n'''
        self.split_str_in_tags = b'8.1/0\n8.1/1000\n8.1/1001\n8.1/1010\n8.1/1011\n8.1/1012\n8.1/1013\n8.1/1014\n8.1/1015\n8.1/1016\n8.1/1017\n8.1/1018\n8.1/1019\n8.1/1040\n8.1/1050\n8.1/1060\n8.1/1070\n8.1/1080\n8.1/Split\n'


    def tearDown(self):
        self.cur_version_tagno = None
        self.pre_version_tagno = None
        self.work_dir = None
        self.html_tag_commits = None

    def test_html_encoding_buildlog(self):
        with patch('buildlog.Popen', autospec=True, spec_set=True) as mocked_popen:
            mocked_popen.return_value.communicate.return_value = (self.html_tag_commits,None)
            gc = buildlog.get_commits(self.cur_version_tagno,self.pre_version_tagno, self.work_dir)
            # test the number of open tags equals close tags
            assert gc.count('<li>') == gc.count('</li>')
            # get commits builds a HTML list, the only appropriate tag is <li></li> - remove for tests
            gc = gc.replace('<li>','')
            gc = gc.replace('</li>','')
            assert '<' not in gc
            assert '>' not in gc

    def test_html_encoding_buildlog_start_end(self):
        with patch('buildlog_start_end.Popen', autospec=True, spec_set=True) as mocked_popen:
            mocked_popen.return_value.communicate.return_value = (self.html_tag_commits,None)
            gc = buildlog_start_end.get_commits(self.cur_version_tagno,self.pre_version_tagno, self.work_dir)
            # test the number of open tags equals close tags
            assert gc.count('<li>') == gc.count('</li>')
            # get commits builds a HTML list, the only appropriate tag is <li></li> - remove for tests
            gc = gc.replace('<li>','')
            gc = gc.replace('</li>','')
            assert '<' not in gc
            assert '>' not in gc

    def test_split_string_in_tags_buildlog(self):
        with patch('buildlog.Popen', autospec=True, spec_set=True) as mocked_popen:
            mocked_popen.return_value.communicate.return_value = (self.split_str_in_tags, None)
            tags = buildlog.get_tags('8.1', 'C:\\Git\\newmv\\magview')
            assert 'Split' not in tags


    def test_split_string_in_tags_buildlog_start_end(self):
        with patch('buildlog.Popen', autospec=True, spec_set=True) as mocked_popen:
            mocked_popen.return_value.communicate.return_value = (self.split_str_in_tags, None)
            tags = buildlog_start_end.get_all_tags_int_list('8.1', 'C:\\Git\\newmv\\magview')
            assert 'Split' not in tags



if __name__ == '__main__':
    unittest.main()
