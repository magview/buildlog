from subprocess import Popen, PIPE
from datetime import datetime
import json
import requests
import sys
import logging
import bleach
import time
import os
import re

tb_html = ''  # This var will store all HTML table body rows (all rows) before set_page_json gets called


def get_tags_from_to(version, work_dir, start_tag, end_tag):
    """Get a list of tags from a certain tag to a certain tag in the list of all tags."""
    tag_list_int = get_all_tags_int_list(version, work_dir)
    tag_list_int.sort(reverse=True)
    try:
        start_tag = int(start_tag)
        end_tag = int(end_tag)
        if start_tag in tag_list_int and end_tag in tag_list_int:
            start_tag_idx = tag_list_int.index(start_tag)
            end_tag_idx = tag_list_int.index(end_tag)
            tag_list_int_from_to = tag_list_int[start_tag_idx:end_tag_idx + 2]
            tag_list_str = [str(i) for i in tag_list_int_from_to]  # convert int list to string list(after sorting)
            return tag_list_str
    except ValueError:
        print('Please enter number(tag number)!!!')
        logging.error('fn_get_tags_from_to: Please enter number(tag number)!!!')
    else:
        print('The tag is not existed!!!')
        logging.error('fn_get_tags_from_to: The tag is not existed!!!')


def get_all_tags_int_list(version, work_dir):
    """Get all the tags from the version requested"""
    git_cmd = 'git tag -l ' + '"' + version + '*"'
    pipe = Popen(git_cmd, shell=True, cwd=work_dir, stdout=PIPE, stderr=PIPE)
    (out, error) = pipe.communicate()

    # logging/error
    if error:
        cmd_err = error.decode('utf-8')
        logging.error(f"shell error: {cmd_err}")
        print(f"git tag cmd error: {cmd_err}", file=sys.stderr)

    result = out.decode('utf-8')
    tag_list = result.split('\n')
    tag_list_len = len(tag_list)
    del tag_list[tag_list_len - 1]
    for i, s in enumerate(tag_list):
        tl = s.split('/')
        tag_list[i] = tl[1]
        # discard Split tag in 8.1
    tag_list = [int(i) for i in tag_list if i != 'Split']  # convert string list to int list(sorting)
    return tag_list


def get_builddate(version_tagno, work_dir):
    """Get the tag build date(release)"""
    git_cmd = 'git show -s --format=%ci ' + version_tagno
    pipe = Popen(git_cmd, shell=True, cwd=work_dir, stdout=PIPE, stderr=PIPE)
    (out) = pipe.communicate()
    result = (out[0]).decode('utf-8')
    builddate = result[:19]
    dt_builddate = datetime.strptime(builddate, '%Y-%m-%d %H:%M:%S')
    str_builddate = f'{dt_builddate:%m/%d/%Y %I:%M:%S%p}'
    return str_builddate


def get_commits(cur_version_tagno, pre_version_tagno, work_dir):
    """Get all the commits for each tag"""
    git_cmd = 'git log ' + pre_version_tagno + '..' + cur_version_tagno + ' --pretty=oneline || sort || uniq'
    pipe = Popen(git_cmd, shell=True, cwd=work_dir, stdout=PIPE, stderr=PIPE)
    (out, error) = pipe.communicate()

    # logging/error
    if error:
        cmd_err = error.decode('utf-8')
        print(f"git log cmd error: {cmd_err}", file=sys.stderr)
        logging.error(f"shell error: {cmd_err}")

    result = out.decode('utf-8')
    commit_list = result.split('\n')
    logging.debug(f'Commit list: {commit_list}')
    commit_list.sort()  # have to sort it again!
    del commit_list[0]
    commit_list = [i[41:] for i in commit_list]  # Remove Prefix (*)
    commit_list = [bleach.clean(i.strip()) for i in commit_list] # strip and escape commits
    for i, s in enumerate(commit_list):
        if s[:1] == '*':
            commit_list_i = s[1:]
        else:
            commit_list_i = commit_list[i]
        commit_list[i] = commit_list_i.replace("<", "(").replace(">", ")").replace("\t", " ").replace("&&", ",").replace("&", ",")

    commit_list_strip = [i.strip() for i in commit_list]  # Remove space at both ends

    if len(commit_list_strip) == 1:
        str_commit_list = commit_list_strip[0]
    else:
        str_commit_list = '</li><li>'.join(commit_list_strip)

    return str_commit_list


def get_page_json(page_id, expand=False):
    """Get the page info, where the BuildLog table is displayed"""
    if expand:
        suffix = "?expand=" + expand
                                #body.storage
    else:
        suffix = ""
    url = "https://magview.atlassian.net/wiki/rest/api/content/" + page_id + suffix
    response = requests.get(url, auth=('move@magview.com', 'S8CGDd744GxclX7QMjn10CF9'))
    response.encoding = "utf-8"

    if response.status_code == 200:
        logging.debug(f"get_page_json http_status:{response.status_code} text:{response.text}")
        
    else:
        print((f"Error getting page id {page_id}"
               f" http status{response.status_code}"
               f" error text {response.json().get('message','buildlog_start_end.py err - Message not found!')}"),
              file=sys.stderr)
        logging.error(f"get_page_json http_status:{response.status_code} text:{response.text}")

    return json.loads(response.text)


def set_page_json(page_id, json_content):
    """Display the BuildLog table"""
    headers = {
        'Content-Type': 'application/json',
    }
    response = requests.put("https://magview.atlassian.net/wiki/rest/api/content/" + page_id, headers=headers, data=json.dumps(json_content), auth=('move@magview.com', 'S8CGDd744GxclX7QMjn10CF9'))
    
    if response.status_code == 200:
        dump_page_response(response)
        logging.debug(f"set_page_json http_status:{response.status_code} text:{response.text}")
        print(f'Response OK: Set page {page_id}')
    else:
        print((f"Error setting page {page_id}"
               f" http status {response.status_code}"
               f" error text {response.json().get('message','buildlog_start_end.py err - Message not found!')}"
               f" Confluence has not been updated!"),
              file=sys.stderr)
        dump_page_response(response)
        logging.error(f"set_page_json http_status:{response.status_code} text:{response.text}")

    return response.text


def create_jsoncontent(json_data):
    """Create BuildLog table in JSON format"""
    new_json_data = dict()
    new_json_data['id'] = json_data['id']
    new_json_data['type'] = json_data['type']
    new_json_data['title'] = json_data['title']
    new_json_data['type'] = json_data['type']
    new_json_data['version'] = {"number": json_data['version']['number'] + 1}
    if 'key' not in json_data:
        new_json_data['key'] = json_data['space']['key']
    else:
        new_json_data['key'] = json_data['key']
    new_json_data['body'] = {'storage': {
        'value': '<p><table><thead><tr><th width="60px">Version</th><th width="140px">Build Date</th><th width="800px">Developer Notes</th></tr></thead><tbody>' + tb_html + '</tbody></table></p>',
        'representation': 'storage'}}
    return new_json_data


def create_tb_row__html(curtag, curtag_builddate, curtag_commits):
    """Create BuildLog table row, adding row by row"""
    # This var will store all HTML table body rows (all rows) before display it
    global tb_html
    tb_html += '<tr><td>' + curtag + '</td><td>' + curtag_builddate + '</td><td><li>' + curtag_commits + '</li></td></tr>'
    return tb_html


def dump_page_response(response_object):
    unique_ts = int(time.time())
    response_dump_fn = f'{unique_ts}_response_dump.log'
    request_dump_fn = f'{unique_ts}_request_dump.log'

    with open(response_dump_fn, 'wb') as f:
        f.write(response_object.text.encode('utf8'))

    with open(request_dump_fn, 'wb') as f:
        f.write(response_object.request.body.encode('utf8'))

    logging.debug(f"Request body logged at file:{request_dump_fn}")
    logging.debug(f"dump page http_status:{response_object.status_code} file:{response_dump_fn}")


def cleanup_log_dumps(max_entries=10):
    file_pattern = r'\d*_(response|request)_dump\.log'
    file_match = re.compile(file_pattern)

    file_list = [f for f in os.listdir('.') if file_match.match(f)]
    file_list = sorted(file_list, reverse=True)
    # include the first entry
    files_to_delete = file_list[max_entries:]

    if files_to_delete:
        for f in files_to_delete:
            os.remove(f)


# **********************************************************************************************************************


def main():
    logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p',
                        filename='buildlog_start_end.log',
                        filemode='a', # append log
                        level=logging.DEBUG)

    logging.info('Started')
    try:
        pj_version = sys.argv[1]
        pj_dir = sys.argv[2]
        conflu_pageid = sys.argv[3]
        start_tag = sys.argv[4]
        end_tag = sys.argv[5]

        # pj_version = '8.0'
        # pj_dir = 'C:/bitbucket/magview'
        # conflu_pageid = '773521413'
        # start_tag = '1961'
        # end_tag = '1960'

        conflu_page_info = get_page_json(conflu_pageid)
        all_tags = get_tags_from_to(pj_version, pj_dir, start_tag, end_tag)

        #Loop through tags
        i = 0
        while i < len(all_tags):
            cur_version_tagno = pj_version + '/' + all_tags[i]
            pre_version_tagno = pj_version + '/' + all_tags[i+1]

            cur_version_tagno_builddate = get_builddate(cur_version_tagno, pj_dir)

            cur_version_tagno_commits = get_commits(cur_version_tagno, pre_version_tagno, pj_dir)

            create_tb_row__html(cur_version_tagno, cur_version_tagno_builddate, cur_version_tagno_commits)

            i += 1

            if i == len(all_tags) - 1:
                break
        #Loop ends

        new_json_data = create_jsoncontent(conflu_page_info)
        set_page_json(conflu_pageid, new_json_data)
    except IndexError:
        print('Pass in five values!\n\t1st Value: Project Version.\n\t2nd Value: Project Directory.\n\t3rd Value: Confluence PageID\n\t4th Value: the latest tag.\n\t5th Value: the last tag.')

    cleanup_log_dumps()
    logging.info('Finished')

main()
