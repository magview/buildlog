
"""
Usage:
  dry_run_utility.py [options] (<repo_directory> <project_key>)
  dry_run_utility.py -v | --version
  dry_run_utility.py -h | --help

Options:
  -h --help      Show this screen
  -v --version   Show Version
  -c --complete  Default is to report changes only.  Use this option to show all tickets extracted, even when status=closed and fixVersion is current.
"""
import pCloseJiraTicket
import os
import buildlog
import logging
from subprocess import Popen, PIPE
from unittest.mock import MagicMock
import copy, csv, sys
try:
    from docopt import docopt
except ImportError:
    print("docopt required!  Please install docopt by way of 'pip install docopt'", file=sys.stderr)

class Error(Exception):
    """Base class for exceptions in this module."""
    pass


class InputError(Error):
    """Exception raised for errors in the input.

    Attributes:
        expression -- input expression in which the error occurred
        message -- explanation of the error
    """

    def __init__(self, expression, message):
        self.expression = expression
        self.message = message

dry_run_utility_version = 'Dry Run Utility .1'


class DryRunpCloseJiraTicket:
    '''
    Perform a dry run of pCloseJiraTicket
    **pCloseJiraTicket.py and buildlog.py must be importable (same dir) as DryRunpCloseJiraTicket **
    No changes to JIRA are made during dry run.

    pCloseJiraTicket.py Required Arguments:
        work_dir: Directory of Git repo
        project_key: JIRA project key [MAG, LUNG]

    DryRunpCloseJiraTicket options:
        version
        pre_tag
        latest_tag

    For a standard dry run of pCloseJiraTicket.py
        1) pass work_dir and project_key

    '''

    def __init__(self,
                 work_dir,
                 project_key,
                 version=None,
                 pre_tag=None,
                 latest_tag=None,
                 changes_only=True):

        self.work_dir = work_dir
        self.project_key = project_key
        self.version = version
        self.pre_tag = pre_tag
        self.latest_tag = latest_tag
        self.iJira = None
        self.version_tags = []
        self.ticket_status = []
        self.list_tickets = []
        self.gather_tags()
        self.release_version = ''
        self.change_summary = []
        self.changes_only = changes_only

    @property
    def work_dir(self):
        return self._work_dir

    @work_dir.setter
    def work_dir(self, val):
        '''
        Verify path exists and is a git repo
        '''
        if os.path.isdir(val):
            cmd = 'git rev-parse --is-inside-work-tree'
            rslt = self.x_cmd(cmd,val)
            if 'true' in rslt:
                self._work_dir = val
            else:
                raise InputError(val, rslt)
        else:
            raise InputError(val, "Not a directory")

    @property
    def project_key(self):
        return self._project_key

    @project_key.setter
    def project_key(self,val):
        '''
        Only MagView project key for now
        '''
        if str(val).upper() in ['MAG']:
            self._project_key = str(val).upper()
        else:
            InputError(val, "Project Key MAG is required")

    @property
    def version(self):
        return self._version

    @version.setter
    def version(self, val):
        '''
        MagView 8.0/8.1 only right now
        '''
        valid_versions = ['8.0', '8.1']
        if val in valid_versions:
            self._version = val
        else:
            self._version = None

    @property
    def pre_tag(self):
        return self._pre_tag

    @pre_tag.setter
    def pre_tag(self, val):
        '''
        Depends on a valid Version
        '''
        if self.version:
            if val:
                vc = self.version + '/'

                if val[:len(vc)] != vc:
                    self._pre_tag = vc + str(val)
                else:
                    self._pre_tag = str(val)
            else:
                self._pre_tag = None
        else:
            self._pre_tag = None

    @property
    def latest_tag(self):
        return self._latest_tag

    @latest_tag.setter
    def latest_tag(self, val):
        '''
        Depends on a valid Version
        '''
        if self.version:
            if val:
                vc = self.version + '/'

                if val[:len(vc)] != vc:
                    self._latest_tag = vc + str(val)
                else:
                    self._latest_tag = str(val)
            else:
                self._latest_tag = None
        else:
            self._latest_tag = None

    def x_cmd(self, any_cmd, work_dir, decode=True):
        '''
        Execute command
        '''
        pipe = Popen(any_cmd, shell=True, cwd=work_dir, stdout=PIPE, stderr=PIPE)
        (out, error) = pipe.communicate()
        if decode:
            if error:
                return error.decode('utf-8')
            return out.decode('utf-8')
        else:
            if error:
                return error
            return out

    def gather_tags(self):
        if self.version and self.pre_tag:
            self.version_tags = buildlog.get_tags(self.version, self.work_dir)
        else:
            self.version_tags = []

    def dry_run(self):
        '''

        '''
        # standard dry run - Close tickets associated with latest tag
        if not self.pre_tag:
            logging.info('Standard scenario started ')

            # Step_1: Get the Release_version number
            assert pCloseJiraTicket.is_valid_git_dir(self.work_dir) # sets project_repo
            self.release_version = pCloseJiraTicket.get_release_version(self.work_dir)
            logging.debug(f'Latest version extracted from Git Repo Directory {self.release_version}')

            # When a version is not specified, use latest tag to set version
            if not self.version:
                self.version = '.'.join(self.release_version.split('.')[0:2])

            # Step_2: Get the Latest tag number && the Previous tag number
            latest_tag, pre_tag = pCloseJiraTicket.get_two_tags(self.work_dir)
            self.pre_tag = pre_tag
            self.latest_tag = latest_tag

        # todo: improve start/end tags
        if self.pre_tag:
            logging.info('Historic scenario started ')
            self.release_version = self.latest_tag.replace('/','.')
            logging.debug(f'Release Version to be Created {self.release_version}')

        logging.debug(f'Grab commits from {self.latest_tag} that are not reachable in {self.pre_tag} ')
        # Step_3: Get Tickets under the release version tag
        list_tickets = pCloseJiraTicket.get_list_tickets(pre_tag=self.pre_tag,
                                                         latest_tag=self.latest_tag,
                                                         work_dir=self.work_dir,
                                                         project_key=self.project_key)
        self.list_tickets = list_tickets
        logging.debug(f'{self.project_key} tickets extracted:{list_tickets} ')

        # Step_4: Connect to Jira
        self.iJira = pCloseJiraTicket.conn_jira()

        # Step_4a - check if release exists to prevent JIRAERROR
        jproject_versions = self.iJira.project_versions(self.project_key)
        if jproject_versions:
            jproject_version_names = [v.name for v in jproject_versions]
            if self.release_version in jproject_version_names:
                logging.warning(f'JiraError release version: {self.release_version} Exists')
            else:
                logging.debug(f'New release version {self.release_version} would be created')

        # Step_5: Create release version in Jira (needed: jira, version number, project key)
        # Dry Run - skipped

        # Step_6: Change Status field && Fix Versions
        # Dry Run - report back
        self.dry_run_tickets()

    def dry_run_tickets(self):
        '''
        Does a dry run (no changes to Jira)
        '''
        # reset dry run tickets
        self.ticket_status = []
        rcd = dict()

        for t in self.list_tickets:
            rcd = dict()

            rcd['ticket'] = t

            try:
                i1 = self.iJira.issue(t)
                rcd['new_status'] = 'Closed'
            except Exception as e:
                rcd['error'] = str(e)
                logging.error(f"JiraError ticket: {t} resulted in err{rcd['error']}")
                logging.warning(f"fix pCloseJiraTicket {t} {self.release_version}.{self.pre_tag}.{self.latest_tag}")

            if not rcd.get('error', None):

                try:
                    rcd['current_status'] = i1.fields.status.name
                except Exception as e:
                    rcd['current_status'] = str(e)

                # type check for error
                rcd['new_status'] = 'Closed'

                try:
                    rcd['current_fixVersions'] = [i.name for i in i1.fields.fixVersions]
                    rcd['new_fixVersions'] = copy.deepcopy(rcd['current_fixVersions'])
                    if self.release_version not in rcd['current_fixVersions']:
                        rcd['new_fixVersions'].append(self.release_version)

                except Exception as e:
                    rcd['current_fixVersions'] = str(e)
                    rcd['new_fixVersions'] = 'unable to update'

            self.ticket_status.append(rcd)

    def report_results(self, csv_output=True):
        '''
        Processing functions
        '''

        def status_or_fixv_filter(d):
            if d.get('error', None) is None:
                if d['current_status'] != d['new_status']:
                    return True
                if d['current_fixVersions'] != d['new_fixVersions']:
                    return True

        def status_flags(d):
            new_d = dict()
            new_d.update(d)
            if d['current_status'] != d['new_status']:
                new_d['status_change'] = True
            if d['current_fixVersions'] != d['new_fixVersions']:
                new_d['fixVersion_change'] = True

            return new_d

        if self.changes_only:
            changed_items = list(filter(status_or_fixv_filter, self.ticket_status))
        else:
            changed_items = self.ticket_status

        self.change_summary = list(map(status_flags, changed_items))

        if csv_output:
            self.write_change_summary_csv()


    def write_change_summary_csv(self):
        '''
        Creates a .csv file report of changes to Jira
        '''
        with open(f'dry_run_{self.release_version}.csv', 'w', newline='') as csv_file:
            fieldnames = ['ticket', 'new_status', 'current_status', 'current_fixVersions',
                          'new_fixVersions', 'status_change', 'fixVersion_change']
            writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
            writer.writeheader()

            for d in self.change_summary:
                writer.writerow(d)


if __name__ == '__main__':

    arguments = docopt(__doc__, version=dry_run_utility_version)

    work_dir = arguments['<repo_directory>']
    logging.debug(f'command line argument work_dir: {work_dir}')
    project_key = arguments['<project_key>']
    logging.debug(f'command line argument project_key: {project_key}')
    changes_only = not arguments["--complete"]
    logging.debug(f'Report changes only: {changes_only}')

    dy = DryRunpCloseJiraTicket(work_dir=work_dir,
                                project_key=project_key,
                                changes_only=changes_only)
    try:
        dy.dry_run()
    except Exception as e:
        logging.critical(f"Unexpected error {str(e)}")

    try:
        dy.report_results(csv_output=True)
        print(f"Success: see {f'dry_run_{dy.release_version}.csv'}", file=sys.stdout)
    except Exception as e:
        logging.critical(f"Unexpected error {str(e)}")