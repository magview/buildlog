from subprocess import Popen, PIPE
from datetime import datetime
import json
import requests
import sys
import logging
import bleach
import time
import re
import os
import dry_run_utility

try:
    from git import Repo, GitCmdObjectDB
    project_repo = None
except ImportError as e:
    error_msg = 'GitPython library not found, please install "pip install GitPython" to continue'
    logging.critical(error_msg)
    raise Exception(error_msg)

tb_html = ''  # This var will store all HTML table body rows (all rows) before set_page_json gets called


def get_tags(version, work_dir):
    """Get all the tags from the version requested"""
    git_cmd = 'git tag -l ' + '"' + version + '*"'
    pipe = Popen(git_cmd, shell=True, cwd=work_dir, stdout=PIPE, stderr=PIPE)
    (out, error) = pipe.communicate()

    # error reporting to stdout
    if error:
        cmd_err = error.decode('utf-8')
        print(f'git tag cmd err: {cmd_err}', file=sys.stderr)
        logging.error(f"shell error: {cmd_err}")

    result = out.decode('utf-8')
    tag_list = result.split('\n')
    tag_list_len = len(tag_list)
    del tag_list[tag_list_len - 1]
    for i, s in enumerate(tag_list):
        tl = s.split('/')
        tag_list[i] = tl[1]
    # Ignore 8.1 historic Split tag
    tag_list = [int(i) for i in tag_list if i != 'Split'] # convert string list to int list(sorting)
    tag_list.sort(reverse=True)
    return tag_list


def get_builddate(version_tagno, work_dir):
    """Get the tag build date(release)"""
    git_cmd = 'git show -s --format=%ci ' + version_tagno
    pipe = Popen(git_cmd, shell=True, cwd=work_dir, stdout=PIPE, stderr=PIPE)
    (out) = pipe.communicate()
    result = (out[0]).decode('utf-8')
    builddate = result[:19]
    dt_builddate = datetime.strptime(builddate, '%Y-%m-%d %H:%M:%S')
    str_builddate = f'{dt_builddate:%m/%d/%Y %I:%M:%S%p}'
    return str_builddate


def extract_tickets(list_of_commits, project_key='MAG'):
    extracted_tickets = []
    regex = re.compile(project_key + '\W\d*', re.IGNORECASE)

    for t in list_of_commits:
        list_result = regex.findall(t)
        if list_result:
            extracted_tickets.extend(list_result)

    return extracted_tickets


def remove_asterisk(list_of_commits):
    new_commits = list()

    for c in list_of_commits:
        if c:
            if c[0] == '*':
                new_commits.append(c[1:].strip())
            else:
                new_commits.append(c)

    return new_commits


def get_commits(cur_version_tagno,
                pre_version_tagno,
                work_dir,
                project_key='MAG',
                merge_filter=True,
                rm_asterisk=True):
    """
    Get commits in cur_version_tagno that are not reachable in pre_version_tagno
    This is different than using 'git log --no-merges'
    Extracts all tickets from commits then checks merges
    Depends on remove_duplicates to remove duplicates from the right side of the merge branch

    """
    project_repo = Repo(work_dir, odbt=GitCmdObjectDB)
    all_commit_list = list(project_repo.iter_commits(rev=f'{pre_version_tagno}..{cur_version_tagno}'))
    if merge_filter:
        mag_ticket_regex = re.compile(project_key + '\W\d*', re.IGNORECASE)
        commit_list = [c.summary for c in all_commit_list if len(c.parents)==1]
        merge_commit_list = [c.summary for c in all_commit_list if
                             len(c.parents) > 1 and
                             bool(mag_ticket_regex.search(c.summary))]
        extracted_ticket_list = extract_tickets(commit_list, project_key)

        to_add = list()
        for mc in merge_commit_list:
            tl = mag_ticket_regex.findall(mc)
            results = list()
            for t in tl:
                results.append(t in extracted_ticket_list )

            if False in results:
                to_add.append(mc)

        # Extend commit list to include merges with tickets that would be otherwise missed (rare)
        commit_list.extend(to_add)
    else:
        commit_list = [c.summary for c in all_commit_list]

    commit_list = [bleach.clean(c) for c in commit_list]
    commit_list_strip = remove_duplicates(commit_list)

    if rm_asterisk:
        commit_list_strip = remove_asterisk(commit_list_strip)

    if len(commit_list_strip) == 1:
        str_commit_list = commit_list_strip[0]
    else:
        str_commit_list = '</li><li>'.join(commit_list_strip)

    return str_commit_list


def remove_duplicates(list_of_commits):
    """
    Removes duplicate commits
    """
    list_of_commits = list(set(list_of_commits))
    return list_of_commits


def get_page_json(page_id, expand = False):
    """Get the page info, where the BuildLog table is displayed"""
    if expand:
        suffix = "?expand=" + expand
                                #body.storage
    else:
        suffix = ""
    url = "https://magview.atlassian.net/wiki/rest/api/content/" + page_id + suffix
    response = requests.get(url, auth=('move@magview.com', 'S8CGDd744GxclX7QMjn10CF9'))
    response.encoding = "utf-8"

    # Error reporting
    if response.status_code == 200:
        logging.debug(f"get_page_json http_status:{response.status_code} ")
    else:
        print((f"Error getting page id {page_id}"
               f" http status{response.status_code}"
               f" error text {response.json().get('message','buildlog.py err - Message not found!')}"),
              file=sys.stderr)
        logging.error(f"get_page_json http_status:{response.status_code} ")

    return json.loads(response.text)


def set_page_json(page_id, json_content):
    """Display the BuildLog table"""
    headers = {
        'Content-Type': 'application/json',
    }
    response = requests.put("https://magview.atlassian.net/wiki/rest/api/content/" + page_id, headers=headers, data=json.dumps(json_content), auth=('move@magview.com', 'S8CGDd744GxclX7QMjn10CF9'))
    if response.status_code == 200:
        logging.debug(f"set_page_json http_status:{response.status_code} text:{response.text}")
        dump_page_response(response)
        print(f'Response OK: Set page {page_id}')
    else:
        dump_page_response(response)
        print((f"Error setting page {page_id}"
               f" http status {response.status_code}"
               f" error text {response.json().get('message','buildlog.py err - Message not found!')}"
               f" Confluence has not been updated!"),
              file=sys.stderr)
        logging.error(f"set_page_json http_status:{response.status_code} text:{response.text}")

    return response.text


def create_jsoncontent(json_data):
    """Create BuildLog table in JSON format"""
    new_json_data = dict()
    new_json_data['id'] = json_data['id']
    new_json_data['type'] = json_data['type']
    new_json_data['title'] = json_data['title']
    new_json_data['type'] = json_data['type']
    new_json_data['version'] = {"number": json_data['version']['number'] + 1}
    if 'key' not in json_data:
        new_json_data['key'] = json_data['space']['key']
    else:
        new_json_data['key'] = json_data['key']
    new_json_data['body'] = {'storage': {
        'value': '<p><table><thead><tr><th width="60px">Version</th><th width="140px">Build Date</th><th width="800px">Developer Notes</th></tr></thead><tbody>' + tb_html + '</tbody></table></p>',
        'representation': 'storage'}}
    # 'value': '<p><table><thead><tr><th width="100px">Version</th><th width="200px">Build Date</th><th width="800px">Developer Notes</th></tr></thead><tbody><tr><td>value1</td><td>value2</td><td>value3</td></tr></tbody></table></p>',
    return new_json_data


def create_tb_row__html(curtag, curtag_builddate, curtag_commits):
    """Create BuildLog table row, adding row by row"""
    # This var will store all HTML table body rows (all rows) before display it
    global tb_html
    tb_html += '<tr><td>' + curtag + '</td><td>' + curtag_builddate + '</td><td><li>' + curtag_commits + '</li></td></tr>'
    return tb_html


def dump_page_response(response_object):
    unique_ts = int(time.time())
    response_dump_fn = f'{unique_ts}_response_dump.log'
    request_dump_fn = f'{unique_ts}_request_dump.log'

    with open(response_dump_fn, 'wb') as f:
        f.write(response_object.text.encode('utf8'))

    with open(request_dump_fn, 'wb') as f:
        f.write(response_object.request.body.encode('utf8'))

    logging.debug(f"Request body logged at file:{request_dump_fn}")
    logging.debug(f"dump page http_status:{response_object.status_code} file:{response_dump_fn}")


def cleanup_log_dumps(max_entries=10):
    file_pattern = r'\d*_(response|request)_dump\.log'
    file_match = re.compile(file_pattern)

    file_list = [f for f in os.listdir('.') if file_match.match(f)]
    file_list = sorted(file_list, reverse=True)
    # include the first entry
    files_to_delete = file_list[max_entries:]

    if files_to_delete:
        for f in files_to_delete:
            os.remove(f)

# **********************************************************************************************************************


def main():
    # pj_version = '8.0'
    # pj_dir = 'C:/bitbucket/magview'
    # conflu_pageid = '773521413'

    logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p',
                        filename='buildlog.log',
                        filemode='a', # append log
                        level=logging.DEBUG)

    logging.info('Started')

    try:
        pj_version = sys.argv[1]
        pj_dir = sys.argv[2]
        conflu_pageid = sys.argv[3]
    except IndexError:
        print('Pass in three values!\n\tFirst: Project Version.\n\tSecond: Project Directory.\n\tThird: Confluence PageID')

    conflu_page_info = get_page_json(conflu_pageid)
    all_tags = get_tags(pj_version, pj_dir)

    #Loop through tags
    i = 0
    while i < len(all_tags):
        cur_version_tagno = f'{pj_version}/{all_tags[i]}'
        pre_version_tagno = f'{pj_version}/{all_tags[i+1]}'

        cur_version_tagno_builddate = get_builddate(cur_version_tagno, pj_dir)

        cur_version_tagno_commits = get_commits(cur_version_tagno, pre_version_tagno, pj_dir)

        create_tb_row__html(cur_version_tagno, cur_version_tagno_builddate, cur_version_tagno_commits)

        i += 1

        if i == len(all_tags) - 1:
            break
        #Loop ends
    if all_tags: # prevent writing a blank page
        new_json_data = create_jsoncontent(conflu_page_info)
        set_page_json(conflu_pageid, new_json_data)

    cleanup_log_dumps()
    logging.info('Finished')

    # run dry run utility
    do_dryrun = sys.argv[1] and sys.argv[2] and sys.argv[3]

    if do_dryrun:
        try:
            logging.info('Start Dry Run utility')
            project_key = 'MAG'
            dy = dry_run_utility.DryRunpCloseJiraTicket(pj_dir,
                                                        project_key,
                                                        version=pj_version,
                                                        pre_tag=f'{pj_version}/{all_tags[1]}',
                                                        latest_tag=f'{pj_version}/{all_tags[0]}')

            dy.dry_run()
            dy.report_results(csv_output=True)
            print(f'See dry_run_{dy.release_version}.csv for changes')
        except Exception as e:
            logging.error(f'Error with Dry Run Utility {str(e)}')

if __name__ == '__main__':
    main()
