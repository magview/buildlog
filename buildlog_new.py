from subprocess import Popen, PIPE
from datetime import datetime
import json, requests, sys, logging, time, os, re
from requests.auth import HTTPBasicAuth
import dry_run_utility
from jinja2 import Template, Environment, escape, FileSystemLoader

from unittest.mock import MagicMock

mock_logging = False
build_log_version = ".25"

if mock_logging:
    logging = MagicMock()
else:
    logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p',
                        filename='buildlog.log',
                        filemode='a', # append log
                        level=logging.DEBUG)

    logging.info(f'Started {build_log_version}')


class BuildLogNew:
    '''
    Buildlog using Jinja2 template
    Can be used for both the major version tag and with no tags
    Designed to be resilient by tracking status of page and following through.
      If the utility fails to update confluence
        Each tag is submitted individually
        Tags that fail are logged

    Commandline Examples:
      Python buildlog_new.py {version} {path} {confluence page id} {start tag} {end tag} {user} {apikey}

      All tags
        python buildlog_new.py 8.0 C:\Git\magview8.0\magview 866156677

      From 129 to 2013 ( inclusive)
        python buildlog_new.py 8.0 C:\Git\magview8.0\magview 866156677 139 2013

      Only 2013:
        python buildlog_new.py 8.0 C:\Git\magview8.0\magview 866156677  2013 2013

      From 2000 on (open end tag):
        python buildlog_new.py 8.0 C:\Git\magview8.0\magview 866156677  2000

      First tag to 1980 (open start tag):
        python buildlog_new.py 8.0 C:\Git\magview8.0\magview 866156677  no_start* 1980
        (*) placeholder tag – can be anything that is not an actual tag
    '''

    def __init__(self, user, apikey, work_dir, version, page_id=0, start_tag=None, end_tag=None ):
        self.sesh = requests.session()
        self.sesh.auth = HTTPBasicAuth(user, apikey)
        self.sesh.headers = {'Content-Type': 'application/json', }

        self.rsp = None
        self.max_log_entries = 10
        self.work_dir = work_dir
        self.version = version
        self.page_id = page_id
        self.start_tag = start_tag
        self.end_tag = end_tag

        self.tag_list = list()
        self.build_log_data = list()
        self.rendered_log = ""

        self.page_json = {}
        self.new_json_page = {}
        self.page_confirmation = {}

        # follow through objects
        self.successes = list()
        self.response_log = dict()
        self.experimental_feature = False

        # initialize functions
        self.get_tags()
        self.validate_start_end_tags()

    @property
    def page_url(self):
        return f"https://magview.atlassian.net/wiki/rest/api/content/{self.page_id}"

    @property
    def start_tag(self):
        return self._start_tag

    @start_tag.setter
    def start_tag(self, val):
        if val:
            self._start_tag = str(val)
        else:
            self._start_tag = None

    @property
    def end_tag(self):
        return self._end_tag

    @end_tag.setter
    def end_tag(self, val):
        if val:
            self._end_tag = str(val)
        else:
            self._end_tag = None

    @property
    def tags_loaded(self):
        if self.tag_list:
            return True
        return False

    ## Git functions ##
    def get_tags(self):
        """Get all the tags from the version requested"""
        git_cmd = 'git tag -l ' + '"' + self.version + '*"'
        pipe = Popen(git_cmd, shell=True, cwd=self.work_dir, stdout=PIPE, stderr=PIPE)
        (out, error) = pipe.communicate()

        # error reporting to stdout
        if error:
            cmd_err = error.decode('utf-8')
            print(f'git tag cmd err: {cmd_err}', file=sys.stderr)
            logging.error(f"shell error: {cmd_err}")

        result = out.decode('utf-8')
        self.tag_list = result.split('\n')
        tag_list_len = len(self.tag_list)
        logging.info(f'tags: {self.tag_list}')
        del self.tag_list[tag_list_len - 1]
        for i, s in enumerate(self.tag_list):
            tl = s.split('/')
            self.tag_list[i] = tl[1]
        self.tag_list = [int(i) for i in self.tag_list] # convert string list to int list(sorting)
        self.tag_list.sort(reverse=True)
        self.tag_list = [str(i) for i in self.tag_list] # convert int list to string list(after sorting)


    def get_builddate(self, version_tagno):
        """
        Get the tag build date(release)
        """
        git_cmd = 'git show -s --format=%ci ' + version_tagno
        pipe = Popen(git_cmd, shell=True, cwd=self.work_dir, stdout=PIPE, stderr=PIPE)
        (out) = pipe.communicate()
        result = (out[0]).decode('utf-8')
        builddate = result[:19]
        dt_builddate = datetime.strptime(builddate, '%Y-%m-%d %H:%M:%S')
        str_builddate = f'{dt_builddate:%m/%d/%Y %I:%M:%S%p}'
        return str_builddate

    def get_commits(self, cur_version_tagno, pre_version_tagno):
        """
        Get all the commits for each tag
        Return a list of commits

        """
        if cur_version_tagno and pre_version_tagno:
            git_cmd = 'git log ' + pre_version_tagno + '..' + cur_version_tagno + ' --no-merges --pretty=format:"%s" || sort || uniq'
        else:# case for the last tag, version 0
            git_cmd = 'git log ' +  cur_version_tagno + ' --no-merges --pretty=format:"%s" || sort || uniq'

        pipe = Popen(git_cmd, shell=True, cwd=self.work_dir, stdout=PIPE, stderr=PIPE)
        (out, error) = pipe.communicate()

        # error reporting to stdout
        if error:
            cmd_err = error.decode('utf-8')
            print(f'git log cmd err: {cmd_err}', file=sys.stderr)
            logging.error(f'git log cmd err: {cmd_err}')

        raw_commits = out.decode('utf-8')
        return self.process_raw_commits(raw_commits)


    def process_raw_commits(self, raw_commits):
        '''
        helper function to format commits
        '''
        commit_list = raw_commits.split('\n')
        logging.debug(f'Commit list: {commit_list}')
        commit_list.sort()
        commit_list = [i.strip() for i in commit_list]
        commit_list = [i for i in commit_list if i != '']

        return commit_list

    ## Tag functions ##
    def validate_start_end_tags(self):
        '''
        Start, end, or both tag(s) can be None
        Start tag should be less than end tag
        '''
        # validate against tag list
        if self.start_tag:
            if str(self.start_tag) not in self.tag_list:
                self.start_tag = None

        if self.end_tag:
            if str(self.end_tag) not in self.tag_list:
                self.end_tag = None

        if self.start_tag and self.end_tag:
            # invalid start/ end tag - both tags are set to None
            if int(self.start_tag) > int(self.end_tag):
                self.start_tag = None
                self.end_tag = None


    def tag_cutoff_list(self):
        '''
         Outputs a list of tuples.  Each tuple is a start,end tag.
         Function get_commits expects inclusive tags
         The last item in the list can have a null end tag for 8.0/0 and 8.1/0
        '''
        tl = self.tag_list

        # Tags in descending order.
        if self.start_tag:
            # Adjust cutoff to be inclusive
            # + 1 for python list slicing behavior
            # + 1 for git log cmd behavior
            start_pos = tl.index(self.start_tag) + 2
            tl = self.tag_list[:start_pos]

        if self.end_tag:
            end_pos = tl.index(self.end_tag)
            tl = tl[end_pos:]

        # add version to tags
        tl = [self.version + '/' + t for t in tl]

        tl = self.tag_list_to_tuples(tl)

        return tl

    def tag_list_to_tuples(self, tag_list):
        # create start/stop tuples from list
        start_stop_tags = list()

        if len(tag_list) == 1:
            start_stop_tags = [(tag_list[0], None)]
        else:
            start_stop_tags = list(zip(tag_list, tag_list[1:]))

        # If there is no start tag, use the last tag in the sorted tag list
        # Check this behavior if the utility is used on products other than MagView/LungView
        if self.start_tag in ['0', None]:
            start_stop_tags.append((self.version + '/' + self.tag_list[-1], None))

        return start_stop_tags

    def build_commit_list(self):
        '''
        This function creates the core data structure used by the utility by:
          Getting the build date and commits for each tag (minor version)
          It usually takes less than 20 seconds to run on 8.0

        '''
        # reset build log data
        if self.build_log_data:
            self.build_log_data = list()

        start_stop_tags = self.tag_cutoff_list()

        for t in start_stop_tags:
            newd = dict()
            newd['version'] = t[0]
            newd['build_date'] = self.get_builddate(t[0])
            newd['commits'] = self.get_commits(t[0], t[1])
            self.build_log_data.append(newd)

        # data functions
        self.remove_duplicates()
        self.remove_astrisk()
        self.capitalize_first_letter()
        self.remove_release_build()
        #self.escape_commits() # escaping is in the template "| e"


    ## Data processing functions ##


    def remove_duplicates(self):
        """
        Removes duplicate commits
        todo: what exactly is causing this issue
        """
        for i, rcd in enumerate(self.build_log_data):
            self.build_log_data[i]['commits'] = list(set(rcd['commits']))


    def remove_astrisk(self):
        '''

        '''
        for i, rcd in enumerate(self.build_log_data):
            new_commits = list()
            for c in self.build_log_data[i]['commits']:
                if c[0] == '*':
                    new_commits.append(c[1:].strip())
                else:
                    new_commits.append(c)
            self.build_log_data[i]['commits'] = new_commits

    def capitalize_first_letter(self):
        ''' '''
        for i, rcd in enumerate(self.build_log_data):
            new_commits = list()
            for c in self.build_log_data[i]['commits']:
                if c[0] in 'abcdefghijklmnopqrstuvwxyz':
                    new_commits.append(c[0].upper() + c[1:])
                else:
                    new_commits.append(c)
            self.build_log_data[i]['commits'] = new_commits

    def remove_release_build(self):
        '''

        '''
        for i, rcd in enumerate(self.build_log_data):
            new_commits = list()
            for c in self.build_log_data[i]['commits']:
                if c[-5:] != 'Build':
                    new_commits.append(c)
                else:
                    # log commit was filtered out of Build Log
                    None
            self.build_log_data[i]['commits'] = new_commits

    ## jinja2 template functions ##

    def render_template(self):
        file_loader = FileSystemLoader('templates')
        env = Environment(loader=file_loader)
        buildlog_template = env.get_template('buildlog_data.html')
        self.rendered_log = buildlog_template.render(build_commits=self.build_log_data)


    ## Confluence ##


    def get_page_json(self):

        """Get the page info, where the BuildLog table is displayed"""
        self.rsp = self.sesh.get(self.page_url)
        self.rsp.encoding = "utf-8"
        # Error reporting
        if self.rsp.ok:
            logging.debug(f"get_page_json http_status:{self.rsp.status_code} text:{self.rsp.text}")
        else:
            print((f"Error getting page id {self.page_id}"
                   f" http status{self.rsp.status_code}"
                   f" error text {self.rsp.json().get('message','buildlog.py err - Message not found!')}"),
                  file=sys.stderr)
            logging.error(f"get_page_json http_status:{self.rsp.status_code} text:{self.rsp.text}")

        self.page_json = self.rsp.json()

    def create_jsoncontent(self):
        """
        Create BuildLog table in JSON format
        Version must be incremented by 1
        """
        self.render_template()

        new_json_data = dict()
        new_json_data['id'] = self.page_json['id']
        new_json_data['type'] = self.page_json['type']
        new_json_data['title'] = self.page_json['title']
        new_json_data['type'] = self.page_json['type']
        new_json_data['version'] = {"number": self.page_json['version']['number'] + 1}

        if 'key' not in self.page_json:
            new_json_data['key'] = self.page_json['space']['key']
        else:
            new_json_data['key'] = self.page_json['key']

        new_json_data['body'] = {'storage': {
            'value': self.rendered_log,
            'representation': 'storage'}}

        self.new_json_page = new_json_data


    def set_page_json(self):
        """
        Puts BuildLog table on Confluence
        Logs results
        """

        self.rsp = self.sesh.put(self.page_url, data=json.dumps(self.new_json_page))
        if self.rsp.ok:
            logging.debug(f"set_page_json http_status:{self.rsp.status_code} ")
            self.dump_page_response()
            print(f'Response OK: Set page {self.page_id}')
        else:
            self.dump_page_response()
            print((f"Error setting page {self.page_id}"
                   f" http status {self.rsp.status_code}"
                   f" error text {self.rsp.json().get('message','buildlog.py err - Message not found!')}"
                   f" Confluence has not been updated!"),
                  file=sys.stderr)
            logging.error(f"set_page_json http_status:{self.rsp.status_code} text:{self.rsp.text}")

        self.page_confirmation = self.rsp.json()

    def cleanup_versions(self, version_to_keep,
                         prior_versions_to_delete,
                         page_id ):
        '''
        Purpose: manage version history
        x versions prior to the cut-off version are deleted

        **No checks are performed prior to delete**
        '''
        start_version = version_to_keep - 1
        dqty = prior_versions_to_delete
        delete_list = [start_version -1*i for i in range(0, prior_versions_to_delete)]
        responses = dict()
        base = 'https://magview.atlassian.net/wiki'

        for dv in delete_list:
            delete_path = f'/rest/api/content/{page_id}/version/{dv}'
            delete_url = base + delete_path
            try:
                responses[dv] = self.sesh.delete(base + delete_path)
            except Exception as e:
                print(f'error deleting version: {dv} {str(e)}')

        if responses[dv].status_code != 204:
            print(f'http error expected 204, actual: {responses[dv].status_code} while deleting version: {dv} ')

        return responses

    ## Logging helper functions ##
    def dump_page_response(self):
        '''
        Logs page response
        '''

        unique_ts = int(time.time())
        response_dump_fn = f'{unique_ts}_response_dump.log'
        request_dump_fn = f'{unique_ts}_request_dump.log'

        with open(response_dump_fn, 'wb') as f:
            f.write(self.rsp.text.encode('utf8'))

        with open(request_dump_fn, 'wb') as f:
            f.write(self.rsp.request.body.encode('utf8'))

        logging.debug(f"Request body logged at file:{request_dump_fn}")
        logging.debug(f"dump page http_status:{self.rsp.status_code} file:{response_dump_fn}")

        self.cleanup_log_dumps()


    def cleanup_log_dumps(self):
        '''
        Deletes response/request logs when max_entries has been reached
        '''
        file_pattern = r'\d*_(response|request)_dump\.log'
        file_match = re.compile(file_pattern)

        file_list = [f for f in os.listdir('.') if file_match.match(f)]
        file_list = sorted(file_list, reverse=True)
        # include the first entry
        files_to_delete = file_list[self.max_log_entries:]

        if files_to_delete:
            for f in files_to_delete:
                os.remove(f)

    ## follow-through
    def extracted_tags_match(self):
        '''
        Compare tags from internal build_log_data object to html returned from confluence
        Returns False if a tag is missing
        '''
        new_page = self.page_confirmation.get('body', {}) \
            .get('storage', {}) \
            .get('value', '')

        expected_versions = [d['version'] for d in self.build_log_data]

        ev_dict = dict()
        for ev in expected_versions:
            ev_dict[ev] = new_page.find(ev) == -1

        if True in ev_dict.values():
            # tag not found
            return False
        return True


    def follow_throuh_page(self):
        '''
        If the tags in the data object does not match confluence (page failed)
          Identify which tag caused an issue
          Follow through by posting all working tags
        '''
        if not self.extracted_tags_match():
            # submit tag by tag
            self.get_page_json()
            start_page_version = self.page_json['version']['number'] + 1
            all_data = self.build_log_data
            # submit by tag
            self.max_log_entries = 10 + len(all_data) # temporarily reset http log dump limit.
            for rcd in all_data:
                self.build_log_data = [rcd]
                self.get_page_json()
                self.create_jsoncontent()
                self.set_page_json()
                self.response_log[rcd['version']] = self.rsp

                if self.extracted_tags_match():
                    self.successes.append(rcd['version'])
                else:
                    logging.error(f"Tag {rcd['version']} Failed")
                    print(f"Tag {rcd['version']} Failed")

            # submit working tags
            self.build_log_data = [d for d in all_data if d['version'] in self.successes]
            self.get_page_json()
            self.create_jsoncontent()
            self.set_page_json()
            self.get_page_json()
            end_version = self.page_json['version']['number']
            self.cleanup_versions(end_version, end_version - start_page_version, self.page_id)


## run as standalone


if __name__ == '__main__':
    try:
        pj_version = sys.argv[1]
        pj_dir = sys.argv[2]
        page_id = sys.argv[3]
    except IndexError:
        print(('Please pass\n'
               'Project Version:\n'
               '\t8.0\n'
               '\t8.1\n'
               'Project Dir:\n'
               '\tPath to MagView Git Repo\n'
               'Confluence Page ID\n'
               '\t8.0.x - 866156677\n'
               '\t8.1.x - 864059402\n'))

    try:
        start_tag = sys.argv[4]
    except IndexError:
        start_tag = None

    try:
        end_tag = sys.argv[5]
    except IndexError:
        end_tag = None

    try:
        user = sys.argv[6]
    except IndexError:
        user = 'move@magview.com'

    try:
        apikey = sys.argv[7]
    except IndexError:
        apikey = 'S8CGDd744GxclX7QMjn10CF9'

    if pj_version and pj_dir and page_id:
        bln = BuildLogNew(user=user,
                          apikey=apikey,
                          work_dir=pj_dir,
                          version=pj_version,
                          page_id=int(page_id),
                          start_tag=start_tag,
                          end_tag=end_tag)

        if bln.tags_loaded:
            bln.build_commit_list()
            bln.get_page_json()
            bln.create_jsoncontent()
            bln.set_page_json()

            if bln.experimental_feature:
                bln.follow_throuh_page()

        else:
            logging.error(f'No tags found in {bln.work_dir} - Build Log NOT attempted.  Check err log for more details')
            print(f"Error No tags found in {bln.work_dir} - Build log NOT attempted")

    logging.info(f'Finished {build_log_version}')

    if pj_version and pj_dir and page_id:
        #try run dry run utility
        try:
            logging.info('Start Dry Run utility')
            project_key = 'MAG'
            dy = dry_run_utility.DryRunpCloseJiraTicket(pj_dir, project_key)
            dy.dry_run()
            dy.report_results(csv_output=True)
            print(f'See dry_run_{dy.release_version}.csv for changes')
        except Exception as e:
            logging.error(f'Error with Dry Run Utility {str(e)}')
