"""
This script: should run after a released version got created in GitKraken.
This will:
1. Create the version in Jira.
2. Close all the tickets under the version, created.

Dependencies:
jira 2.0
  https://pypi.org/project/jira/

git is accessible from cmd
"""
from subprocess import Popen, PIPE
from datetime import date
import re, sys, os, logging

logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p',
                    filename='pCloseJiraTicket.log',
                    filemode='a',  # append log
                    level=logging.DEBUG)

logging.info(f'{"+" * 100}')
logging.info('Starting pCloseJiraTicket.py')
logging.info(f'{"+" * 100}')

## check if external dependencies are installed.
try:
    from jira import JIRA, JIRAError
except ImportError as e:
    error_msg = 'jira library not found, please install "pip install jira" to continue'
    logging.critical(error_msg)
    raise Exception(error_msg)

try:
    from git import Repo, GitCmdObjectDB
    project_repo = None
except ImportError as e:
    error_msg = 'GitPython library not found, please install "pip install GitPython" to continue'
    logging.critical(error_msg)
    raise Exception(error_msg)

# **********************************************************************************************************************
#                          FUNCTIONS
# **********************************************************************************************************************

def is_valid_git_dir(work_dir):
    """Tests if workdir is valid and is a git repo"""
    is_valid = False
    if os.path.isdir(work_dir):
        logging.debug(f'directory {work_dir} is valid')

        cmd = 'git rev-parse --is-inside-work-tree'
        pipe = Popen(cmd, shell=True, cwd=work_dir, stdout=PIPE, stderr=PIPE)
        result, error = pipe.communicate()
        not_a_repo_msg = 'not a git repository'
        not_a_cmd_msg = 'not recognized as an internal or external command'

        if error:
            # limitation - this will not catch the wrong directory level
            if not_a_cmd_msg in error.decode('utf-8'):
                logging.critical(f'Git is not accessible from shell!  Please install git cmd.')
            if not_a_repo_msg in error.decode('utf-8'):
                logging.critical(f'Git repo not found! Please pass git repo as first argument')
        else:
            logging.debug(f'{work_dir} is a valid Git repo')
            is_valid = True
            global project_repo
            project_repo = Repo(work_dir, odbt=GitCmdObjectDB)
            assert project_repo.working_tree_dir == work_dir, "Git repo working tree mis-match!"
    else:
        logging.critical(f'{work_dir} is not a valid directory')

    return is_valid


def get_two_tags(work_dir):
    """
    Get the latest tag and previous tag
    Sorted by date then alphabetically to return adjacent tag

    """
    global project_repo
    latest_tag = sorted([t for t in project_repo.tags], key=lambda t: t.commit.committed_datetime, reverse=True)[0]
    pre_tag_idx = [t.name for t in project_repo.tags].index(latest_tag.name) - 1
    pre_tag = [t for t in project_repo.tags][pre_tag_idx]
    return latest_tag.name, pre_tag.name


def get_release_version(work_dir):
    """Get the release version tag, across all branches"""
    global project_repo
    latest_tag = sorted([t for t in project_repo.tags], key=lambda t: t.commit.committed_datetime, reverse=True)[0]
    return latest_tag.name.replace('/', '.')


def format_latest_tag(str_value):
    """Replace '/' with '.' """
    tag_list = str_value.split('\n')
    latest_tag = tag_list[0].replace('/', '.')
    return latest_tag


def get_list_tickets(pre_tag, latest_tag, work_dir, project_key):
    """Get all the tickets under the released version tag, that needed to be closed"""
    git_cmd = 'git log ' + pre_tag + '..' + latest_tag + ' --pretty=format:"%s" --grep=' + project_key + '-'
    pipe = Popen(git_cmd, shell=True, cwd=work_dir, stdout=PIPE, stderr=PIPE)
    (result, error) = pipe.communicate()
    str_notes = result.decode('utf-8')
    list_str_notes = str_notes.split('\n')

    # Extract ticket numbers from the commit messages
    list_tickets: str = []
    for t in list_str_notes:
        regex = re.compile(project_key + '\W\d*', re.IGNORECASE)
        list_result = regex.findall(t)

        if list_result:
            # Another option, capture last ticket from commit message
            #   and all tickets from merge branches
            list_tickets.extend(list_result)

    list_tickets = remove_duplicate_tickets(list_tickets)
    list_tickets = ticket_check(list_tickets)

    return list_tickets


def remove_duplicate_tickets(input_list):
    '''
        see example 8.1/1060 mag-9157 and MAG-9157
    '''
    return list(set([e.upper() for e in input_list]))


def ticket_check(ticket_list):
    '''
    Second level check for valid tickets for any project.
    Key must be two to ten uppercase letters followed by a dash and one to eight numbers
        This function will  filter incomplete 'MAG-' from 8.1/1040 and log action
    :param: ticket_list
    :return: filtered ticket list
    '''
    ticket_pat = re.compile('[A-Z]{2,10}-[0-9]{1,8}')
    checked_tickets = list()
    for t in ticket_list:
        if ticket_pat.match(t):
            checked_tickets.append(t)
        else:
            logging.warning(f'Ticket {t} removed from list')

    return checked_tickets


def conn_jira():
    """Connect to Jira"""
    jira_options = {'server': 'https://magview.atlassian.net'}
    jira = JIRA(options=jira_options, basic_auth=('move@magview.com', 'S8CGDd744GxclX7QMjn10CF9'))
    return jira


def create_version(jira, release_version, project_key):
    """Create the released version"""
    try:
        jira.create_version(release_version, project_key, releaseDate=str(date.today()), released=True)
        print(f'Created Version {release_version}', file=sys.stdout)
    except JIRAError as e:
        err_msg = f"Issue creating release version {release_version} for {project_key}| {e.text} | {e.status_code}"
        logging.error(err_msg)
        print(err_msg, file=sys.stderr)


def close_tickets(jira, list_tickets, release_version):
    """Change the ticket Status && Add Fix Versions"""
    summary = dict()
    summary['errors'] = 0
    summary['fixVersion'] = 0
    summary['closed'] = 0

    if not list_tickets:
        return
    else:
        i = 0
        while i < len(list_tickets):
            try:
                issue = jira.issue(list_tickets[i])
            except JIRAError as e:
                issue = None
                err_msg = f"Issue with ticket {list_tickets[i]} | {e.text} | {e.url} | {e.status_code}"
                logging.error(err_msg)
                print(err_msg, file=sys.stderr)
                summary['errors'] += 1

            if issue:
                # STATUS: [('81', 'Closed'), ('101', 'To Do'), ('31', 'Uploaded For Review'), ('51', 'In Testing')]
                ticket_status = str(issue.fields.status)
                logging.debug(f'{list_tickets[i]}: present status {ticket_status}')

                if ticket_status != 'Closed':
                    try:
                        jira.transition_issue(issue, '81')
                        logging.debug(f'{list_tickets[i]} status changed to Closed')
                        summary['closed'] += 1
                    except JIRAError as e:
                        err_msg = f"Failed to update ticket {list_tickets[i]} | {e.text} | {e.url} | {e.status_code}"
                        logging.error(err_msg)
                        print(err_msg, file=sys.stderr)
                        summary['errors'] += 1

                # FIX VERSION:
                ticket_fix_version = []
                for version in issue.fields.fixVersions:
                    ticket_fix_version.append(version.name)

                if not ticket_fix_version:
                    issue.add_field_value('fixVersions', {'name': release_version})
                    logging.debug(f'{release_version} was added to {list_tickets[i]}')
                    summary['fixVersion'] += 1
                else:
                    if release_version not in ticket_fix_version:
                        issue.add_field_value('fixVersions', {'name': release_version})
                        logging.debug(f'{release_version} was added to existing fixVersions in {list_tickets[i]}')
                        summary['fixVersion'] += 1

            i += 1
    return summary


# **********************************************************************************************************************


if __name__ == '__main__':
    # PASS IN PARAMETER *********************
    # work_dir = 'C:\bitbucket\magview'
    # project_key = 'MAG'
    # todo: special case for MAG.  Branches 8.0 and 8.1 are hardcoded.  Adjust when updated.
    # ***************************************

    try:
        work_dir = sys.argv[1]
        logging.debug(f'command line argument work_dir: {work_dir}')
        project_key = sys.argv[2].upper()
        logging.debug(f'command line argument project_key: {project_key}')
    except IndexError:
        raise Exception('Pass in two values!\n\tFirst: Project Directory.\n\tSecond: Project Key(e.g. MAG).')

    # test if work_dir is a valid git repo
    if not is_valid_git_dir(work_dir):
        raise Exception(f"Work Directory Argument {work_dir} is not a Valid Git Directory")

    # Step_1: Get the Release_version number
    release_version = get_release_version(work_dir)
    logging.debug(f'Release version: {release_version}')

    # Step_2: Get the Latest tag number && the Previous tag number
    latest_tag, pre_tag = get_two_tags(work_dir)
    logging.debug(f'Latest tag: {latest_tag}, previous tag: {pre_tag}')

    # Step_3: Get Tickets under the release version tag
    list_tickets = get_list_tickets(pre_tag, latest_tag, work_dir, project_key)
    logging.debug(f'extracted_tickets {list_tickets}')

    # Step_4: Connect to Jira
    jira = conn_jira()

    # Step_5: Create release version in Jira (needed: jira, version number, project key)
    create_version(jira, release_version, project_key)

    # Step_6: Change Status field && Fix Versions
    action_summary = close_tickets(jira, list_tickets, release_version)
    if action_summary:
        summary_msg = (f'Complete:\n'
                       f'Tickets Closed:      {action_summary["closed"]}\n'
                       f'fixVersions Updated: {action_summary["fixVersion"]}\n'
                       f'Errors:              {action_summary["errors"]}')

        print(summary_msg, file=sys.stdout)
    else:
        print('No Changes', file=sys.stdout)

    logging.info('Finished pCloseJiraTicket.py')
