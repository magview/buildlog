import unittest
import pCloseJiraTicket
from unittest.mock import patch

class TestTagRegex(unittest.TestCase):
    def setUp(self):
        self.pre_tag = "test_tag"
        self.latest_tag = "test_tag"
        self.work_dir = "test_dir"
        self.project_key = 'MAG'
        self.lowercase_mag1 = b'Merged in bugfix/MAG-8499-patch-mag-8229-to-8.0.1962 (pull request #527)'
        self.lowercase_mag2 = b'Merged in MAG-8752-patch-mag-8500-to-8.0.1980 (pull request #690)'
        self.lung_project = b'Removed MagView, BI-RADS anb MAMMO related items in LungView system. LUNG-75 (CW, PI)'
        self.mdt_project = b'automated iPerf bandwidth checking to System Information screen (BDO, PR) MDT-555'
        self.test_msg = b'Added richlist for section layouts (TMC PN) [t:3874, s:jira] MAG-MAG-3874'
        self.test_msg2 = b' focus on procdure date on pathology screen. (PK, C5/A2/S1/R3?PR) [t:5290, s:jira] MAG- 5290'
        self.test_dup = b'increment RC for MAG-8273 going into 196 MAG-8273'
        self.no_tag = b'No tag in commit message MA_100 zzz TexT'

    def tearDown(self):
        self.pre_tag = None
        self.latest_tag = None
        self.work_dir = None
        self.project_key = None

    def test_no_tag(self):
        with patch('pCloseJiraTicket.Popen', autospec=True, spec_set=True) as mocked_popen:
            mocked_popen.return_value.communicate.return_value = (self.no_tag, None)
            list_tickets = pCloseJiraTicket.get_list_tickets(self.pre_tag,
                                                             self.latest_tag,
                                                             self.work_dir,
                                                             self.project_key)
            assert list_tickets == []

    def test_partial_mag_ticket(self):
        with patch('pCloseJiraTicket.Popen', autospec=True, spec_set=True) as mocked_popen:
            mocked_popen.return_value.communicate.return_value = (self.test_msg, None)
            list_tickets = pCloseJiraTicket.get_list_tickets(self.pre_tag,
                                                             self.latest_tag,
                                                             self.work_dir,
                                                             self.project_key)
            assert 'MAG-' not in list_tickets
            assert list_tickets != []
            assert 'MAG-3874' in list_tickets

    def test_space_in_ticket(self):
        with patch('pCloseJiraTicket.Popen', autospec=True, spec_set=True) as mocked_popen:
            mocked_popen.return_value.communicate.return_value = (self.test_dup, None)
            list_tickets = pCloseJiraTicket.get_list_tickets(self.pre_tag,
                                                             self.latest_tag,
                                                             self.work_dir,
                                                             self.project_key)
            assert 'MAG-8273' in list_tickets
            assert len(list_tickets) == 1

    def test_lowercase_mag_ticket(self):
        with patch('pCloseJiraTicket.Popen', autospec=True, spec_set=True) as mocked_popen:
            mocked_popen.return_value.communicate.return_value = (self.lowercase_mag1, None)
            list_tickets = pCloseJiraTicket.get_list_tickets(self.pre_tag,
                                                             self.latest_tag,
                                                             self.work_dir,
                                                             self.project_key)

            assert 'MAG-8229' in list_tickets
            assert '' not in list_tickets
            assert list_tickets != []

    def test_two_mag_ticket(self):
        with patch('pCloseJiraTicket.Popen', autospec=True , spec_set=True) as mocked_popen:
            mocked_popen.return_value.communicate.return_value = (self.lowercase_mag2, None)
            list_tickets = pCloseJiraTicket.get_list_tickets(self.pre_tag,
                                                             self.latest_tag,
                                                             self.work_dir,
                                                             self.project_key)
            assert 'MAG-8752' in list_tickets
            assert 'MAG-8500' in list_tickets

    def test_project_key(self):
        with patch('pCloseJiraTicket.Popen', autospec=True , spec_set=True) as mocked_popen:
            mocked_popen.return_value.communicate.return_value = (self.lung_project, None)
            list_tickets = pCloseJiraTicket.get_list_tickets(self.pre_tag,
                                                             self.latest_tag,
                                                             self.work_dir,
                                                             self.project_key)
            assert list_tickets == []



if __name__ == '__main__':
    unittest.main()
