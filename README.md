
## This Repo Includes Three Utilities 
* buildlog.py
    * Extracts and summarizes commit message from a project
    * Posts summary table to confluence
* buildlog_start_end.py
    * Extracts and produces a summary table like buildlog
    * Accepts start/end tag parameters to limit scope
* pCloseJiraTicket.py
    * Transitions jira ticket status
    * Provides a dry run of ticket transitions without making changes
    
### Example of Buildlog Table Output to Confluence

| Version  | Build Date            | Developer Notes                                                                   |
|----------|-----------------------|-----------------------------------------------------------------------------------|
| 8.1/1080 | 05/06/2020 01:56:30PM | added audit type EQRC and bugfixes MAG-9521                                       |
|          |                       | NQMBC Measures 51-56 fixed cutoff in the value field MAG-9381                     |
|          |                       | NQMBC2 Add filter to measure 50 on report completeness MAG-9520                   |

## Requirements
* Download and install [Python 3](https://www.python.org/downloads/) to run buildlog.py
* Download and install [pip](https://pip.pypa.io/en/stable/installing/), python's package manager
* Install required packages
     * Checkout this repo
     * Open a command prompt
     * Navigate to the repo
     * run command ```pip install -r requirements.txt```

## Buildlog Usage Information 

#### Existing Confluence Page IDs:
* 1031768429 - [test buildlog page](https://magview.atlassian.net/wiki/spaces/MVBUILD/pages/1031768429)
* 864059402 - [MagView 8.1.x](https://magview.atlassian.net/wiki/spaces/MVBUILD/pages/864059402)
* 866156677 - [MagView 8.0.x](https://magview.atlassian.net/wiki/spaces/MVBUILD/pages/866156677)
* You can create a page, at this time Confluence's old editor is the only supported editor

#### buildlog.py required parameters (3)
* __version__: examples - (8.0/8.1)
* __product dir__: Git repo path C:\Git\MagviewRepoDir
* __page id__: Confluence page id

__Example of running the buildlog.py__:```$python^ buildlog.py^^ 8.1 c:/bitbucket/magview 866156677``` <br>
    ^ the path to python.exe (unless you set system varibles path to the file) <br>
   ^^ the path to buildlog.py (unless you set system varibles path to the file) <br>

#### buildlog_start_end.py required parameters (5)
* __version__: examples - (8.0/8.1)
* __product dir__: Git repo path C:\Git\MagviewRepoDir
* __page id__: Confluence page id
* __end_tag__:1020
* __start_tag__:1015

__Example of running buildlog_start_end.py__:```$python* buildlog_start_end** 8.1 c:/bitbucket/magview 864059402 1020 1015```<br>
    ^ the path to python.exe (unless you set system varibles path to the file) <br>
   ^^ the path to buildlog_start_end.py (unless you set system varibles path to the file) <br>

## pCloseJiraTicket.py Usage Information
* Please make sure you checkout the tag you want to work on before running the utility
* It will create a version in JIRA, if the version does not exist
* Tickets are extracted from the checked out tag using the project key in the regex
* All the tickets in that version will be closed by:
    * Change the ticket status to closed
    * Add the version number to the ticket fix versions, if it is not already present

#### pCloseJiraTicket.py required parameters (2)
* __Project Directory:__ path to git repo with tag or branch checked out
* __Project Key:__ e.g. MAG

__Example of running pCloseJiraTicket.py:__```$python^ pCloseJiraTicket.py^^ c:/bitbucket/magview MAG``` <br>
^ the path to python.exe (unless you set system varibles path to the file)<br>
^^ the path to buildlog.py (unless you set system varibles path to the file)

 
 
 



